sed -i "s/{hotspur_host}/${HOTSPUR_HOST}/g" /etc/nginx/conf.d/nginx.conf
sed -i "s/{hotspur_port}/${HOTSPUR_PORT}/g" /etc/nginx/conf.d/nginx.conf
sed -i "s/{hotspur_app_name}/${HOTSPUR_APP_NAME}/g" /etc/nginx/conf.d/nginx.conf

nginx -g "daemon off;"
