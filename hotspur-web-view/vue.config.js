module.exports = {
  "publicPath": "/web-view/",
  "devServer": {
    "port": 80,
		"disableHostCheck": true,
  },
  "transpileDependencies": [
    "vuetify"
  ]
}