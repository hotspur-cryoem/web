import Vue from "vue";
import pouchDB from "pouchdb";


// var design_docs = {
//   _id: "_design/hotspur_web",
//   views: {
//     micrograph: {
//       map: function mapFun(doc) {
//         if (doc.type === "acquisition_data") {
//           // eslint-disable-next-line
//           emit(doc.time, doc.base_name);
//         }
//       }.toString()
//     },
//     acquisition_data: {
//       map: function mapFun(doc) {
//         if (doc.type === "acquisition_data") {
//           // eslint-disable-next-line
//           emit(doc.time, doc.id);
//         }
//       }.toString()
//     },
//     motion_correction_data: {
//       map: function mapFun(doc) {
//         if (doc.type === "motion_correction_data") {
//           // eslint-disable-next-line
//           emit(doc.time, doc.id);
//         }
//       }.toString()
//     },
//     ctf_data: {
//       map: function mapFun(doc) {
//         if (doc.type === "ctf_data") {
//           // eslint-disable-next-line
//           emit(doc.time, doc.id);
//         }
//       }.toString()
//     },
//     montage_data: {
//       map: function mapFun(doc) {
//         if (doc.type === "montage_data") {
//           // eslint-disable-next-line
//           emit(doc.time, doc.id);
//         }
//       }.toString()
//     }
//   }
// };


export const store = {

  // State
  // **********************************************************************************

  data: {
    acquisition: {},
    motion: {},
    ctf: {},
    //map: {},
    //user: {},
    dogpicker: {}
  },
  state: {
    host: `${window.location.protocol}//${window.location.hostname}:${window.location.port}`,

    project: {
      valid: false,
      hash: null,
      remote_db: null,
      sessions: []
    },

    session: {
      valid: false,
      hash: null,
      data: {},
      local_db: null,
      remote_db: null,
      sync_handler1: null,
      sync_handler2: null,
    },

    navigator: null,

    atlas: null,

    squares: {
      route: null,
      latest: null,
      sorted_names: [],
      count: 0,
    },

    micrographs: {
      route: null,
      latest: null,
      sorted_names: [],
      count: 0,
    },

    dogpicker: {
      show: false,
      size: 0,
      thresholds: { min: undefined, max: undefined },
      threshold_minmax: { min: undefined, max: undefined },
      std_thresholds: { min: undefined, max: undefined },
      std_threshold_minmax: { min: undefined, max: undefined },
      data: {},
      picks_to_draw: []
    },

    statistics: {
      hover: null,
      selected: {},
      selected_plot: null,
      x_transform: null
    },

    data: {
      acquisition: {},
      motion: {},
      ctf: {},
      map: {},
      user: {},
      dogpicker: {}
    },
  },


  // Session
  // **********************************************************************************


  load_session(hash, next) {
    if (this.state.session.hash === hash) {
      console.log(`Session ${hash} is already loaded`)
      next();
      return;
    }

    Vue.set(this.state.session, 'hash', hash);
    var remote_db_url = `${this.state.host}/couchdb/${hash}/`
    // skip_setup prevents pouchdb from making database if it doesn't exist
    var remote_db = new pouchDB(remote_db_url, { skip_setup: true });
    // check if remote exists
    remote_db.info()
      .then(info => {
        if (info.error) return Promise.reject(info.error);
      })
      .then(() => {
        console.log(`Verified database ${hash} exists`);
        if (this.state.session.local_db == null) return;
        console.log(`Deleting old local db: ${this.state.session.local_db.name}`);
        return this.state.session.local_db.destroy()
          .then(() => this.state.session.sync_handler1.cancel())
          .then(() => this.state.session.sync_handler2.cancel())
          .catch(err => console.log(`Failed to remove old db due to: "${err}". This may cause web-view slowdows.`))
      })
      .then(() => {
        console.log('Initializing new session');
        this.init_session();
        console.log('Creating local and remote db services');
        var local_db = new pouchDB(hash);
        Vue.set(this.state.session, 'local_db', local_db);
        Vue.set(this.state.session, 'remote_db', remote_db);
      })
      .then(() => {
        this.sync_dbs(this.state.session.local_db, this.state.session.remote_db);
      })
      .catch(err => {
        console.log(`Error connecting to remote session database ${hash}: "${err}"`);
        this.state.session.valid = false;
      })
      .finally(() => next());
  },


  sync_dbs(local, remote) {
    local.allDocs({
      include_docs: true
    }).then(result => {
      console.log("Starting tracking")
      result.rows.forEach(row => {this.handle_data_doc(row.doc)});
      console.log("Tracking done")
      local.changes({
        live: true,
        since: 'now',
        include_docs: true,
        limit: 1000
      }).on("change", (change) => {
        this.handle_data_doc(change.doc)
      });
      console.log("Tracking changes in local db");
      this.state.session.sync_handler1 = local.replicate.from(remote, {
        live: true,
        retry: true,
        batch_size: 500,
        descending: true
      });
      //Make sure only user_data is synced to remote database
      this.state.session.sync_handler2 = local.replicate.to(remote, {
        filter: function (doc) {
          return doc.type === 'user_data' || doc.type === 'user_session_data';
        },
        live: true,
        retry: true,
        batch_size: 500
      });
      console.log("Started syncing remote and local db");
      
    })
    
    
    
  },


  handle_data_doc(doc) {
    switch (doc.type) {
      case null:
        break;

      case "session_data":
        Object.getOwnPropertyNames(doc).forEach(key => {
          Vue.set(this.state.session.data, key, doc[key]);
        });
        break;

      case "navigator_data":
        Object.getOwnPropertyNames(doc).forEach(key => {
          Vue.set(this.state.navigator, key, doc[key]);
        });
        this.track_navigator(doc);
        break;

      case 'montage_data':
        Vue.set(this.state.data.map, doc.base_name, doc);
        break;

      case 'acquisition_data':
        Vue.set(this.state.data.acquisition, doc.base_name, doc);
        this.data.acquisition[doc.base_name] = doc;
        this.track_micrograph(doc)
        break;

      case 'motion_correction_data':
        Vue.set(this.state.data.motion, doc.base_name, doc);
        break;

      case 'ctf_data':
        Vue.set(this.state.data.ctf, doc.base_name, doc);
        break;

      case 'user_data':
        Vue.set(this.state.data.user, doc.base_name, doc);
        if (!('excluded' in this.state.data.user[doc.base_name])) {
          Vue.set(this.state.data.user[doc.base_name], 'excluded', false);
        }
        break;

      case 'dogpicker_data':
        Vue.set(this.state.data.dogpicker, doc.base_name, doc);
        break;

      case 'user_session_data':
        this.track_user_session_data(doc);
        break;

      default:
        break
    }
  },
  binaryInsert(value, array, startVal, endVal) {

    var length = array.length;
    var start = typeof(startVal) != 'undefined' ? startVal : 0;
    var end = typeof(endVal) != 'undefined' ? endVal : length - 1;//!! endVal could be 0 don't use || syntax
    var m = start + Math.floor((end - start)/2);
    
    if(length == 0){
      array.push(value);
      return;
    }
  
    if(this.data.acquisition[value].time > this.data.acquisition[array[end]].time){
      array.splice(end + 1, 0, value);
      return;
    }
  
    if(this.data.acquisition[value].time < this.data.acquisition[array[start]].time){//!!
      array.splice(start, 0, value);
      return;
    }
  
    if(start >= end) {
      return;
    }
  
    if(this.data.acquisition[value].time < this.data.acquisition[array[m]].time){
      this.binaryInsert(value, array, start, m - 1);
      return;
    }
  
    if(this.data.acquisition[value].time > this.data.acquisition[array[m]].time){
      this.binaryInsert(value, array, m + 1, end);
      return;
    }
  
    //we don't insert duplicates
  },

  track_navigator(doc) {
    if (doc.squares) {
      var squares = doc.squares.slice(0)
      squares = squares.reverse() // List is given most recent first. Reverse for same ordering as micrographs
      Vue.set(this.state.squares, 'sorted_names', squares);
      var count = squares.length;
      Vue.set(this.state.squares, 'count', count);
      Vue.set(this.state.squares, 'latest', squares[count - 1]);
    }

    Vue.set(this.state, 'atlas', doc.atlas);
  },


  track_micrograph(doc) {
    if (this.state.micrographs.sorted_names.includes(doc.base_name)) return;

    this.binaryInsert(doc.base_name, this.state.micrographs.sorted_names);
    //this.state.micrographs.sorted_names.push(doc.base_name);
    //this.state.micrographs.sorted_names.sort((a, b) => {
    //  var t1 = this.data.acquisition[a].time;
    //  var t2 = this.data.acquisition[b].time;
    //  return t1 - t2;
    //});

    var count = this.state.micrographs.sorted_names.length;
    Vue.set(this.state.micrographs, 'count', count);
    var latest = this.state.micrographs.sorted_names[count - 1];
    Vue.set(this.state.micrographs, 'latest', latest);
  },

  track_user_session_data(doc) {
    if ('dogpicker_state' in doc && doc.dogpicker_state) {
      if ('size' in doc.dogpicker_state) {
        store.state.dogpicker.size = doc.dogpicker_state.size
      }
      if ('thresholds' in doc.dogpicker_state) {
        if ('min' in doc.dogpicker_state.thresholds) {
          store.state.dogpicker.thresholds.min = doc.dogpicker_state.thresholds.min
        }
        if ('max' in doc.dogpicker_state.thresholds) {
          store.state.dogpicker.thresholds.max = doc.dogpicker_state.thresholds.max
        }
      }
      if ('threshold_minmax' in doc.dogpicker_state) {
        if ('min' in doc.dogpicker_state.threshold_minmax) {
          store.state.dogpicker.threshold_minmax.min = doc.dogpicker_state.threshold_minmax.min
        }
        if ('max' in doc.dogpicker_state.threshold_minmax) {
          store.state.dogpicker.threshold_minmax.max = doc.dogpicker_state.threshold_minmax.max
        }
      }

      if ('std_thresholds' in doc.dogpicker_state) {
        if ('min' in doc.dogpicker_state.std_thresholds) {
          store.state.dogpicker.std_thresholds.min = doc.dogpicker_state.std_thresholds.min
        }
        if ('max' in doc.dogpicker_state.std_thresholds) {
          store.state.dogpicker.std_thresholds.max = doc.dogpicker_state.std_thresholds.max
        }
      }
      if ('std_threshold_minmax' in doc.dogpicker_state) {
        if ('min' in doc.dogpicker_state.std_threshold_minmax) {
          store.state.dogpicker.std_threshold_minmax.min = doc.dogpicker_state.std_threshold_minmax.min
        }
        if ('max' in doc.dogpicker_state.std_threshold_minmax) {
          store.state.dogpicker.std_threshold_minmax.max = doc.dogpicker_state.std_threshold_minmax.max
        }
      }
    }

  },


  init_session() {
    Vue.set(this.state.session, 'valid', true);
    Vue.set(this.state.session, 'data', {});

    Vue.set(this.state, 'navigator', {});

    Vue.set(this.state, 'atlas', null);

    Vue.set(this.state.squares, 'route', null);
    Vue.set(this.state.squares, 'latest', null);
    Vue.set(this.state.squares, 'sorted_names', null);
    Vue.set(this.state.squares, 'count', 0);

    Vue.set(this.state.micrographs, 'route', null);
    Vue.set(this.state.micrographs, 'latest', null);
    Vue.set(this.state.micrographs, 'sorted_names', []);
    Vue.set(this.state.micrographs, 'count', 0);

    Vue.set(this.state.data, 'acquisition', {});
    Vue.set(this.state.data, 'motion', {});
    Vue.set(this.state.data, 'ctf', {});
    Vue.set(this.state.data, 'map', {});
    Vue.set(this.state.data, 'user', {});
  },


  // Project
  // **********************************************************************************


  load_project(hash, next) {
    if (this.state.project.hash === hash) {
      console.log(`Project ${hash} is already loaded`)
      next();
      return;
    }

    this.init_project(hash);

    var remote_db_url = `${this.state.host}/couchdb/${hash}/`
    var remote_db = new pouchDB(remote_db_url, { skip_setup: true });
    remote_db.info()
      .then((info) => {
        if (info.error) return Promise.reject(info.error);
        Vue.set(this.state.project, 'remote_db', remote_db);
        return remote_db.get("project_data");
      })
      .then(doc => {
        var sessions = doc.sessions;
        Vue.set(this.state.project, 'sessions', sessions);
      })
      .catch(err => {
        console.log(`Error connecting to remote session database ${hash}: "${err}"`);
        Vue.set(this.state.project, 'valid', false);
      })
      .finally(() => next())
  },


  init_project(hash) {
    Vue.set(this.state.project, 'hash', hash);
    Vue.set(this.state.project, 'sessions', []);
    Vue.set(this.state.project, 'valid', true);
    Vue.set(this.state.project, 'remote_db', null);
  },


  // Navigation
  // **********************************************************************************


  cache_micrograph_route(route) {
    Vue.set(this.state.micrographs, 'route', route);
  },

  cache_square_route(route) {
    Vue.set(this.state.squares, 'route', route);
  },


  // Tracking changes in user-data
  // ***********************************************************************************

  edit_user_data(micrograph, callback) {
    callback(this.state.data.user[micrograph]);
    var doc = store.state.session.local_db.get(this.state.data.user[micrograph]._id).then(function (doc) {
      callback(doc);
      store.state.session.local_db.put(doc);
    });
  }
}



// This regularly saves certain aspects of the local state into the local session DB, so it will be saved and synced
function update_if_nescessary(the_object, key, value) {
  if (key in the_object && the_object[key] === value) {
    return false;
  } else {
    the_object[key] = value;
    console.log("Updated " + key + " with " + value);
    return true;
  }
}

setInterval(function () {
  if (store.state.session.valid) {
    var changes = false;
    var doc = store.state.session.local_db.get('user_session_data').then(function (doc) {
      if (store.state.dogpicker.size) {
        if (!doc.dogpicker_state || !doc.dogpicker_state.thresholds || !doc.dogpicker_state.std_thresholds) {
          doc.dogpicker_state = { size: null, thresholds: {}, threshold_minmax: {}, std_thresholds: {}, std_threshold_minmax: {} };
          changes = true;
        }
        changes += update_if_nescessary(doc.dogpicker_state, 'size', store.state.dogpicker.size);
        changes += update_if_nescessary(doc.dogpicker_state.thresholds, 'min', store.state.dogpicker.thresholds.min);
        changes += update_if_nescessary(doc.dogpicker_state.thresholds, 'max', store.state.dogpicker.thresholds.max);
        changes += update_if_nescessary(doc.dogpicker_state.threshold_minmax, 'min', store.state.dogpicker.threshold_minmax.min);
        changes += update_if_nescessary(doc.dogpicker_state.threshold_minmax, 'max', store.state.dogpicker.threshold_minmax.max);
        changes += update_if_nescessary(doc.dogpicker_state.std_thresholds, 'min', store.state.dogpicker.std_thresholds.min);
        changes += update_if_nescessary(doc.dogpicker_state.std_thresholds, 'max', store.state.dogpicker.std_thresholds.max);
        changes += update_if_nescessary(doc.dogpicker_state.std_threshold_minmax, 'min', store.state.dogpicker.std_threshold_minmax.min);
        changes += update_if_nescessary(doc.dogpicker_state.std_threshold_minmax, 'max', store.state.dogpicker.std_threshold_minmax.max);
      }

      if (changes) {
        console.log("Pusing User Session Data")
        store.state.session.local_db.put(doc);
      }
    });
  }
}, 2000);