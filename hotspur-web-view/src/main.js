import Vue from "vue";
import App from "./App.vue";
import {store} from "./store.js"
import {router} from "./router.js"
import VueShortkey from "vue-shortkey"
import vuetify from './plugins/vuetify';

Vue.use(VueShortkey)

window.store = store;

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
