import Vue from "vue";
import Router from 'vue-router'
import {store} from './store'

import DataNotLoaded from './views/DataNotLoaded.vue'
import ProjectOverviewApp from './views/ProjectOverviewApp.vue'
import StatisticsApp from './views/StatisticsApp.vue'
import Atlas from './views/Atlas.vue'
import Square from './views/Square.vue'
import MicrographApp from './views/MicrographApp.vue'

Vue.use(Router);

const routes = [

  // Home
  // **********************************************************************************

  {
    path: '/',
    redirect: { name: 'home' },
  },
  {
    path: '/home',
    name: 'home',
    component: DataNotLoaded
  },

  // Project
  // **********************************************************************************

  {
    path: '/project/help',
    name: 'projectHelp',
    component: DataNotLoaded
  },
  {
    path: '/project/:hash',
    name: 'project',
    component: ProjectOverviewApp,
    beforeEnter: (to, from, next) => {
      store.load_project(to.params.hash, next);
    },
  },

  // Statistics
  // **********************************************************************************

  {
    path: '/session/help/statistics',
    name: 'sessionHelp',
    component: DataNotLoaded,
  },
  {
    path: '/session/help',
    redirect: to => {
      return { name: 'sessionHelp' }
    }
  },
  {
    path: '/session/:hash/statistics',
    name: 'statistics',
    component: StatisticsApp,
    beforeEnter: (to, from, next) => {
      store.load_session(to.params.hash, next);
    }
  },
  {
    path: '/session/:hash',
    redirect: to => {
      return {
        name: 'statistics',
        params: { hash: to.params.hash }
      }
    }
  },

  // Atlas
  // **********************************************************************************

  {
    path: '/session/help/atlas',
    name: 'atlasHelp',
    component: DataNotLoaded
  },
  {
    path: '/session/:hash/atlas',
    name: 'atlas',
    component: Atlas,
    beforeEnter: (to, from, next) => {
      store.load_session(to.params.hash, next);
    }
  },

  // Square
  // **********************************************************************************

  {
    path: '/session/help/square',
    name: 'squareHelp',
    component: DataNotLoaded
  },
  {
    path: '/session/:hash/square/latest',
    name: 'squareLatest',
    component: Square,
    beforeEnter: (to, from, next) => {
      store.load_session(to.params.hash, next);
    }
  },
  {
    path: '/session/:hash/square/micrograph',
    name: 'squareMicrograph',
    component: Square,
    beforeEnter: (to, from, next) => {
      store.load_session(to.params.hash, next);
    }
  },
  {
    path: '/session/:hash/square/:name',
    name: 'square',
    component: Square,
    beforeEnter: (to, from, next) => {
      store.load_session(to.params.hash, next);
    }
  },
  {
    path: '/session/:hash/square',
    redirect: to => {
      return {
        name: 'squareLatest',
        params: { hash: to.params.hash }
      }
    }
  },

  // Micrograph
  // **********************************************************************************

  {
    path: '/session/help/micrograph',
    name: 'micrographHelp',
    component: DataNotLoaded,
  },
  {
    path: '/session/:hash/micrograph/latest',
    name: 'micrographLatest',
    component: MicrographApp,
    beforeEnter: (to, from, next) => {
      store.load_session(to.params.hash, next);
    }
  },
  {
    path: '/session/:hash/micrograph/:name',
    name: 'micrograph',
    component: MicrographApp,
    beforeEnter: (to, from, next) => {
      store.load_session(to.params.hash, next);
    }
  },
  {
    path: '/session/:hash/micrograph',
    redirect: to => {
      return {
        name: 'micrographLatest',
        params: { hash: to.params.hash }
      }
    }
  },
];

const router = new Router({
    routes, // short for `routes: routes`
    mode: 'history',
    base: '/web-view/'
});

export { router };
