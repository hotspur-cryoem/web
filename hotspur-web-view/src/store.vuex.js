const store = new Vuex.Store({
    state: {
        couchdb_url: "http://" + window.location.hostname + "/couchdb/",

        session_hash: "",
        valid_session: false,
        remote_session_db: undefined,
        local_session_db: undefined,
    
        project_sessions: [],
    
        session_data: null,
    
        micrographs: [],
        micrographs_by_name: {},
        micrograph_index: null,
        follow_latest: true,
    
        selected_micrograph: {
          acquisition_data: {},
          moction_correction_data: {},
          ctf_data: {},
        },
    
        notification_text: "",
        notification_visible: false,
    },
    mutations: {

    }
})