import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
	},
	theme: {
		dark: true
	}
  // theme:{
  //   primary: "#3d4a57",
  //   secondary: "#f2f2f2",
  //   accent: "#ffc107",
  //   error: "#ff5722",
  //   warning: "#ff9800",
  //   info: "#ffeb3b",
  //   success: "#4caf50"
  // }
});
