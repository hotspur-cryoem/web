This is the code used to build the docker images for Hotspur.

If you're trying to use Hotspur, you'll most likely be interacting with the built docker image on Docker Hub. Running docker-compose should pull these images down automatically, so you hopefully don't need to worry about the code here at all.

If you're doing any development, the dev.docker-compose.yml file will let you use the webpack dev server with hot reloading inside the docker environment. This makes it easy to set up a dev environment (you just need Docker Compose: all dependencies are inside Docker containers), and allows testing as close to the final product as possible during development.